const { MongoClient, ObjectId } = require('mongodb');

const getConnectionURL = ({ username, password, database, host, port }) => username
  ? `mongodb://${encodeURIComponent(username)}:${encodeURIComponent(password)}@${host}:${port}/${database}`
  : `mongodb://${host}:${port}`;

const createConnector = ({
  username = process.env.DBUSER,
  password = process.env.DBPASS || '',
  database = process.env.DBNAME || 'admin',
  host = process.env.DBHOST || 'localhost',
  port = process.env.DBPORT || '27017',
  options = JSON.parse(process.env.DBOPTS || '{}'),
  url = process.env.DBURL || getConnectionURL({ username, password, database, host, port }),
  client = new MongoClient(url, Object.assign({ useNewUrlParser: true, useUnifiedTopology: true }, options)),
} = {}, conn = null) => async () => client.isConnected() ? conn : (conn = await client.connect());

const isId = id => typeof id === 'number' || typeof id === 'string' || id instanceof ObjectId;

const createCRUDFromOptions = ({ collection, connector = createConnector(), database = process.env.DBNAME }) => {
  if (typeof database !== 'string' || !database.trim()) throw new TypeError('Must provide the database name to create a CRUD');
  if (typeof collection !== 'string' || !collection.trim()) throw new TypeError('Must provide the collection name to create a CRUD');

  const conn = async () => (await connector()).db(database.trim()).collection(collection.trim());

  return {
    async create(document) {
      return (await conn()).insertOne(document), document._id;
    },
    async read(queryOrDocId = {}, { fields, skip = 0, limit = 100, ...options } = {}) {
      const projection = Array.isArray(fields) ? fields.reduce((acc, field) => (acc[field] = 1, acc), {}) : fields;
      if (Array.isArray(options.sort)) {
        options.sort = options.sort.reduce((sortArg, field) => (
          sortArg[field.replace(/(?:^[+-]|\s+(?:asc|desc)$)/i, '')] = /(?:^-|\sdesc$)/i.test(field) ? -1 : 1, sortArg
        ), {})
      }
      if (isId(queryOrDocId) || queryOrDocId && isId(queryOrDocId._id)) {
        return (await conn()).findOne({ _id: queryOrDocId._id || queryOrDocId }, { projection, ...options });
      }
      return (await conn()).find(queryOrDocId, { projection, skip, limit, ...options }).toArray();
    },
    async update(queryOrDocOrId, docFragment = queryOrDocOrId) {
      let result;
      if (docFragment === queryOrDocOrId) {
        result = (await conn()).replaceOne({ _id: queryOrDocOrId._id }, docFragment);
      } else {
        const update = Object.entries(docFragment).reduce((acc, [key, val]) => (
          key.startsWith('$') ? (acc[key] = val) : (acc.$set[key] = val), acc
        ), { $set: docFragment.$set || {} });
        if (isId(queryOrDocOrId) || queryOrDocOrId && isId(queryOrDocOrId._id)) {
          result = (await conn()).updateOne({ _id: queryOrDocOrId._id || queryOrDocOrId }, update);
        } else {
          result = (await conn()).updateMany(queryOrDocOrId, update);
        }
      }
      return (await result).modifiedCount;
    },
    async delete(queryOrDocOrId) {
      let result;
      if (isId(queryOrDocOrId) || queryOrDocOrId && isId(queryOrDocOrId._id)) {
        result = (await conn()).deleteOne({ _id: queryOrDocOrId._id || queryOrDocOrId });
      } else {
        result = (await conn()).deleteMany(queryOrDocOrId);
      }
      return (await result).deletedCount;
    }
  };
};

const createCRUD = (connector, database, collection) =>
  createCRUDFromOptions(typeof connector === 'function' ? { connector, database, collection } : connector);

module.exports = { createCRUD, createConnector, getConnectionURL };
